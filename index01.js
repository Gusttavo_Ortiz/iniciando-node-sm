const { response } = require('express');
let express = require('express');
const { request } = require('http');
let app = express();
const port = 8000;

//Recurso principal
app.get('/',(request,response)=>{
 response.send("{message: método get}");
});

//Recurso funcionario
app.get('/funcionario',(request,response)=>{
    let obj = request.query;
    let nome = obj.nome;
    let sobreNome = obj.sobreNome;
    response.send("{message: método get funcionario}"+ nome + sobreNome);
   });

//habilitando o serviço na porta 8000
app.listen(port,function(){
    console.log("Projeto rodando na porta: " + port);
});